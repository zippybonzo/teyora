/**
 * This contains the data types for support, and also what every Wiki features by default if they don't have
 * any overrides.
 */


export enum FeatureSupportStatus {
    NoSupport = 0, // Feature does not work, no support present by us or in the community
    PartialSupport = 1, // Feature has basic support, such as blocking without any templates, or reverting without any templates
    IncompleteSupport = 2, // Feature has some more advanced support, but it's not complete with every template
    ThirdPartySupport = 3, // Feature is fully available, thanks to a third party extension
    FirstPartySupport = 4, // First party extension support or verified Third Party Support
}

export interface WikiSupportMap {
    canBeUsedToAutoApprove?: boolean; // Checks if the wiki can be used for elegibility and auto-approval
    supportsORES?: FeatureSupportStatus;
    supportsWarning?: FeatureSupportStatus;
    supportsReporting?: FeatureSupportStatus;
    supportsBlocking?: FeatureSupportStatus;
    supportsRevert?: FeatureSupportStatus;
    supportsDiscussions?: FeatureSupportStatus;
    supportsTalkPageMessages?: FeatureSupportStatus;
}

export interface WikiSupportInfo {
    langCode?: string;
    langName: string; // The name of the language, e.g. "English"
    langNameEN?: string; // The name of the language in English, e.g. "English"
    name: string; // The name of the wiki, e.g. "Wikipedia"
    url: string; // The URL of the wiki, e.g. "https://en.wikipedia.org"
    supportMap?: WikiSupportMap; // The support map for this wiki
}

export const defaultWikiSupportMap: WikiSupportMap = {
    canBeUsedToAutoApprove: true,
    supportsORES: FeatureSupportStatus.NoSupport,
    supportsWarning: FeatureSupportStatus.NoSupport,
    supportsReporting: FeatureSupportStatus.NoSupport,
    supportsBlocking: FeatureSupportStatus.PartialSupport, // Basic support is just blocking without any templates
    supportsRevert: FeatureSupportStatus.PartialSupport, // Basic support is just reverting without any templates
    supportsDiscussions: FeatureSupportStatus.NoSupport,
    supportsTalkPageMessages: FeatureSupportStatus.FirstPartySupport,
};