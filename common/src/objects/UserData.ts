interface UserData {
    name : string;

    missing? : boolean;

    userid? : number;
    groups? : string[];
    editcount? : number;
}

export default UserData;