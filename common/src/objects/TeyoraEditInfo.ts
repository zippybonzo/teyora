/* eslint-disable camelcase */
/**
 * Interface that holds the format that Teyora uses for edits and related enumerations.
 **/

export enum TeyoraEditType {
    Edit = "edit",
    New = "new",
    Move = "move",
    Delete = "delete",
    Undelete = "undelete",
    Protect = "protect",
    Unprotect = "unprotect",
    Hide = "hide",
    Unhide = "unhide",
    Block = "block",
    Unblock = "unblock",
}

export interface TeyoraEditInfo {
    wiki: string; // The wiki the edit was made on
    page: string; // The page the edit was made on
    type: TeyoraEditType; // The type of edit
    revID: number; // The revision ID of the edit
    previousRevID: number; // The revision ID of the previous edit
    teyoraRevID: string; // The associated Teyora revision ID (i.e. enwiki_12345)
    user: MediaWikiUser;   // The user who made the edit
    editSummary: string; // The edit summary
    editSummaryFormatted: string; // The edit summary, formatted
    timestamp: string; // The timestamp of the edit
    byteDifference: number; // The difference in bytes between the edit and the previous revision
    oresInfo: Record<string, any>; // The ORES info - need an actual interface for this
    minor: boolean; // Whether the edit was minor
    live: boolean; // Whether this came from a live (true) or static (false) feed
    other: Record<string, any>; // Other info for different types of edits - an interface for this would be nice
}

// IMPORTANT LICENSING INFORMATION:
// This user code is courtesy of Chlod's wikimedia-streams package:
// https://github.com/ChlodAlejandro/wikimedia-streams/blob/master/src/streams/common/User.ts
// License:
// https://github.com/ChlodAlejandro/wikimedia-streams/blob/master/LICENSE
// END IMPORTANT LICENSING INFORMATION

export interface MediaWikiUser {
    // THIS IS A MEDIAWIKI USER, NOT A TEYORA USER
    /**
     * The user id that performed this change. This is optional, and will
     * not be present for anonymous users.
     */
    user_id?: number;

    /** The text representation of the user that performed this change. */
    user_text: string;

    /** A list of the groups this user belongs to. E.g. bot, sysop etc. */
    user_groups: string[];

    /**
     * True if this user is considered to be a bot at the time of this
     * event. This is checked via the $user->isBot() method, which
     * considers both user_groups and user permissions.
     */
    user_is_bot: string

    /**
     * The datetime of the user account registration. Not present for
     * anonymous users or if missing in the MW database.
     */
    user_registration_dt?: string;

    /**
     * The number of edits this user has made at the time of this event.
     * Not present for anonymous users.
     */
    user_edit_count?: string;

}