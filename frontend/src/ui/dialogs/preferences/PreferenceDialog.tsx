import { Close } from "@mui/icons-material";
import { Box, Button, Dialog, DialogTitle, IconButton, Stack, Tab, Tabs, Tooltip, Typography } from "@mui/material";
import * as React from "react";
import { withTranslation } from "react-i18next";
import Teyora, { ITYWindowProps } from "../../../App";
import i18n from "../../../i18n";
import { LocalisedButtons } from "../../LocalisedButtons";
import TeyoraUI from "../../TeyoraUI";
import { RenderPreference } from "./Preference";

interface TabContainerProps {
    children?: React.ReactNode;
    index: number;
    value: number;
  }

class PreferenceDialog extends React.Component<ITYWindowProps> {
    state = {
        currentTab: 0
    };

    handleChange = (event: React.ChangeEvent<unknown>, newValue: number) => {
        this.setState({ currentTab: newValue });
    };

    tabContainer(props: TabContainerProps) {
        const { children, index, value, ...other } = props;
        return (
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`vertical-tabpanel-${index}`}
                aria-labelledby={`vertical-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Box sx={{ p: 3 }}>
                        <Typography>{children}</Typography>
                    </Box>
                )}
            </div>
        );
    }

    render() {
        const { t } = this.props; // These are filled in by the withTranslation() decorator.
        const lB = LocalisedButtons();
        return (
            <Dialog open={this.props.open} maxWidth="md" fullWidth={true}>
                <DialogTitle sx={{ m: 0, p: 2 }}>
                    {t("ui:preferences.title")}
                    {!Teyora.TY.CurrentUser.isApproved && (
                        <Typography variant="caption" sx={{paddingLeft: "1vh"}}>
                            {t("ui:preferences.notApproved")}
                        </Typography>
                    )}
                    <Tooltip title={lB.CLOSE}>
                        <IconButton onClick={this.props.onClose} sx={{float: "right"}}>
                            <Close />
                        </IconButton>
                    </Tooltip>
                </DialogTitle>
                <Box sx={{ flexGrow: 1, display: "flex", height: "60vh" }}>
                    <Tabs
                        orientation="vertical"
                        value={this.state.currentTab}
                        onChange={this.handleChange}
                        variant="scrollable"
                        scrollButtons="auto"
                    >
                        {Teyora.TY.RenderablePreferences.map((pref, index) => {
                            if (!pref) return; // We can't render things that are falsey.
                            return (
                                <Tab
                                    key={index}
                                    label={pref.name}
                                />
                            );
                        })}
                    </Tabs>
                    {Teyora.TY.RenderablePreferences.map((prefGroup, index) => {
                        if (!prefGroup) return; // We can't render things that are falsey.
                        return (
                            <this.tabContainer key={index} index={index} value={this.state.currentTab}>
                                {prefGroup.preferences.map((pref, index) => {
                                    return (
                                        <RenderPreference key={index} pref={pref} prefGroupId={prefGroup.groupId} />
                                    );
                                })}
                            </this.tabContainer>
                        );
                    })}
                </Box>

                <Stack
                    direction="row"
                    justifyContent="right"
                    alignItems="flex-end"
                    spacing={2}
                    sx={{
                        p: 2,
                        mt: 2,
                        mb: 0,
                    }}
                >
                    <Button variant="contained" onClick={
                        async () => {
                            if (Teyora.TY.PreferencesManager.PreferenceChangesQueue.length < 1) {
                                TeyoraUI.showAlertBox(t("ui:preferences.noChanges"));
                                return;
                            }

                            // Save the changes.
                            Teyora.TY.SetFullPageLoaderOpen(true);
                            await Teyora.TY.PreferencesManager.commitPreferences();
                            window.location.reload();
                        }
                    }>
                        {t("ui:preferences.save")}
                    </Button>
                </Stack>
                    
                
            </Dialog>
        );
    }
}

export default withTranslation()(PreferenceDialog);