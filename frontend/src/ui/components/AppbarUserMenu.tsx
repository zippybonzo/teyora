// An icon used in appbars that lets users access a menu

import { AdminPanelSettings, Settings, Logout } from "@mui/icons-material";
import { IconButton, Menu, MenuItem, Stack, Typography, ListItemIcon, ListItemText } from "@mui/material";
import React from "react";
import { useTranslation } from "react-i18next";
import Teyora from "../../App";
import { LocalisedButtons } from "../LocalisedButtons";
import TeyoraUI from "../TeyoraUI";
import ProfilePicture from "./profile/ProfilePicture";

export default function AppbarUserMenu(): JSX.Element {
    const { t } = useTranslation();
    const lB = LocalisedButtons();
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  
    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <IconButton
                size="large"
                onClick={handleMenu}
            >
                <ProfilePicture
                    username={Teyora.TY.CurrentUser.username}
                    size={40}
                    profilePicture={Teyora.TY.CurrentProfile.profile.profilePicture}
                />
            </IconButton>
            <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                }}
                transformOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                }}
            >
                {/* Profile information - TODO: on click open pref profile section */}
                <MenuItem onClick={handleClose}>
                    <Stack direction={"row"} spacing={2} >
                        <ProfilePicture
                            username={Teyora.TY.CurrentUser.username}
                            size={40}
                            profilePicture={Teyora.TY.CurrentProfile.profile.profilePicture}
                        />
                        <Stack>
                            <Typography variant="h6">
                                {TeyoraUI.getUserRef()}
                            </Typography>
                            <Typography variant="body2">
                                {/* Teyora user since... */}
                                {t("ui:profile.userSince", {date: new Date(Teyora.TY.CurrentUser.creationDate).toLocaleDateString(undefined, {year: "numeric", month: "long"})})}
                            </Typography>
                        </Stack>
                    </Stack>
                </MenuItem>
                {/* Administration Link (only shown if CurrentUser is administrator) */}
                {Teyora.TY.CurrentUser.isAdmin &&
            <MenuItem onClick={()=>{
                Teyora.TY.OpenAdminHome();
            }}>
                <ListItemIcon>
                    <AdminPanelSettings />
                </ListItemIcon>
                <ListItemText primary={t("ui:admin.openAdminPanel")} />
            </MenuItem>
                }

                {/* Settings dialog link */}
                <MenuItem onClick={()=>{
                    // Open settings dialog
                    TeyoraUI.openPreferences();
                    handleClose();
                }}>
                    <ListItemIcon>
                        <Settings fontSize="small" />
                    </ListItemIcon>
                    <ListItemText>{t("ui:userMenu.settings")}</ListItemText>
                </MenuItem>

                {/* Log out link */}
                <MenuItem onClick={()=>TeyoraUI.confirmLogout()}>
                    <ListItemIcon>
                        <Logout fontSize="small" />
                    </ListItemIcon>
                    <ListItemText>{lB.LOGOUT}</ListItemText>
                </MenuItem>
            </Menu>
        </>
    );
}