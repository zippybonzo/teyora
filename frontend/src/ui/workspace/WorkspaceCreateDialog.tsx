// A basic dialog that creates a new, empty workspace.
// The rest of the configuration happens in the actual workspace settings.
import { Close } from "@mui/icons-material";
import { LoadingButton } from "@mui/lab";
import { Dialog, DialogContent, DialogTitle, IconButton, Stack, TextField, Tooltip } from "@mui/material";
import * as React from "react";
import { useTranslation } from "react-i18next";
import Teyora from "../../App";
import InputTitleWithHelp from "../components/InputTitleWithHelp";
import { LocalisedButtons } from "../LocalisedButtons";


interface WorkspaceCreateDialogProps {
    open: boolean;
    onClose: () => void;
}

export default function WorkspaceCreateDialog(props: WorkspaceCreateDialogProps): JSX.Element {
    // Name: 10-50 characters, Description: 10-1500 characters
    const { t } = useTranslation();
    const { open, onClose } = props;
    const lB = LocalisedButtons();

    const [name, setName] = React.useState("");
    const [nameError, setNameError] = React.useState("");
    const [description, setDescription] = React.useState("");
    const [descriptionError, setDescriptionError] = React.useState("");
    const [creating, setCreating] = React.useState(false);

    return (
        <Dialog open={open} onClose={onClose} fullWidth={true} maxWidth="sm">
            <DialogTitle>
                {t("ui:workspaceSelector.create.title")}
                <Tooltip title={lB.CLOSE}>
                    <IconButton onClick={props.onClose} sx={{float: "right"}}>
                        <Close />
                    </IconButton>
                </Tooltip>
            </DialogTitle>

            <DialogContent>
                <Stack spacing={1}>
                    <InputTitleWithHelp title={t("ui:workspaceSelector.create.details")} help={t("ui:workspaceSelector.create.detailsHelp")} />
                    <TextField
                        label={t("ui:workspaceSelector.create.name")}
                        fullWidth={true}
                        required={true}
                        value={name}
                        error={nameError !== ""}
                        disabled={creating}
                        onChange={(e) => {
                            setName(e.target.value);
                            // If the name is longer than 40 characters, it's too long
                            if (e.target.value.length > 50) {
                                setNameError(t("ui:workspaceSelector.create.nameTooLong"));
                            } else if (e.target.value.length < 10) {
                                setNameError(t("ui:workspaceSelector.create.nameTooShort"));
                            } else {
                                setNameError("");
                            }
                        }}
                        helperText={(nameError || t("ui:workspaceSelector.create.nameHelp"))}
                    />
                    <TextField
                        label={t("ui:workspaceSelector.create.description")}
                        fullWidth={true}
                        multiline={true}
                        rows={4}
                        value={description}
                        error={descriptionError !== ""}
                        required={true}
                        disabled={creating}
                        onChange={(e) => {
                            setDescription(e.target.value);
                            // If the description is longer than 1500 characters, it's too long
                            if (e.target.value.length > 1500) {
                                setDescriptionError(t("ui:workspaceSelector.create.descriptionTooLong"));
                            } else if (e.target.value.length < 10) {
                                setDescriptionError(t("ui:workspaceSelector.create.descriptionTooShort"));
                            } else {
                                setDescriptionError("");
                            }
                        }}
                        helperText={(descriptionError || t("ui:workspaceSelector.create.descriptionHelp"))}
                    />

                    <LoadingButton
                        variant="contained"
                        color="primary"
                        loading={creating}
                        loadingIndicator={t("ui:workspaceSelector.create.creating")}
                        disabled={name.length > 50 || name.length < 10 || description.length > 1500 || description.length < 10 || nameError !== "" || descriptionError !== ""}
                        onClick={async () => {
                        // Verify that the name is shorter than 51 characters but longer than 10 characters
                            if (name.length > 50 || name.length < 10) return;
                            // Verify that the description is shorter than 1501 characters but longer than 10 characters
                            if (description.length > 1500 || description.length < 10) return;
                            // If there are no errors, create the workspace
                            setCreating(true);

                            try {
                                await Teyora.TY.WorkspaceManager.newWorkspace(name, description);
                                setName("");
                                setDescription("");
                                setNameError("");
                                setDescriptionError("");
                                setCreating(false);
                                onClose();

                                // Todo: Add open workspace functionality
                            } catch (error) {
                                console.error(error);
                                setCreating(false);
                            }
                            
                        }}>
                        {t("ui:workspaceSelector.create.create")}
                    </LoadingButton>
                </Stack>
            </DialogContent>
        </Dialog>
    );
}