// The page that is displayed while the patrol is loading
import { Popover, Button, Container, LinearProgress, Typography, Stack, Box } from "@mui/material";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { LocalisedButtons } from "../../LocalisedButtons";
import Teyora from "../../../App";
import { TeyoraPatrol } from "./TeyoraPatrol";

let hasStartedLoading = false;
function startLoading() {
    if (!hasStartedLoading) {
        hasStartedLoading = true;
        Teyora.TY.KeepSplashOpen = true;
        Teyora.TY.WorkspaceManager.loadWorkspace();
    }
}

export default function PatrolLoading() {
    // This is all overlayed over the loading splash screen
    // Ref for div
    return (
        <LoadingStatusPopover />
    );
}

function LoadingStatusPopover() {

    const { t } = useTranslation();

    const lB = LocalisedButtons();

    // Popover open state
    const [open, setOpen] = React.useState(true);
    const [error, setError] = React.useState<string | undefined>(undefined);

    const [loadingState, setLoadingState] = React.useState<string>(
        t("patrol:loading.status.gettingWorkspace")
    );

    Teyora.TY.WorkspaceManager.onLoadStatusChanged = (status: string) => {
        setLoadingState(status);
    };

    Teyora.TY.WorkspaceManager.onLoadError = async (error: string) => {
        setError(error);
    };

    Teyora.TY.WorkspaceManager.onLoadFinishedHandler = () => {
        setOpen(false);
        setTimeout(() => {
            Teyora.TY.KeepSplashOpen = false;
            document.getElementById("loading").classList.remove("active");
            TeyoraPatrol.TP.ready();
        }, 750);
    };


    startLoading();

    
    return (
        <Popover
            open={open}
            anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
            }}

            // slow it down
            TransitionProps={{
                timeout: {
                    enter: 500,
                    exit: 500,
                },
            }}

            sx={{
                zIndex: 9999999, // Keep above loading splash
                maxWidth: "30rem"
            }}

            anchorReference="anchorPosition"
            anchorPosition={{ top: 0, left: 0 }}
        >
            <Container sx={{
                p: 2,
            }}>
                <Stack spacing={1}>
                    {error ? (
                        <>
                            <Typography variant="h6">
                                {t("patrol:loading.error.title")}
                            </Typography>
                            <Typography variant="body1">
                                {error}
                            </Typography>
                            <Button variant="outlined" onClick={()=>{
                                window.location.reload();
                            }}>
                                {lB.RELOAD}
                            </Button>
                            <Button onClick={()=>{
                                window.location.href = "/";
                            }}>
                                {lB.CANCEL}
                            </Button>
                        </>
                    ): (
                        <>
                            <Box>
                                <Typography variant="h6">{t("patrol:loading.title")}</Typography>
                                <Typography variant="body1" noWrap>
                                    {loadingState}
                                </Typography>
                            </Box>
                        
                            <LinearProgress sx={{
                                width: "25rem"
                            }}/>

                            <Button onClick={()=>{
                                // This takes us back to the workspace selector
                                Teyora.TY.LinkHandler.setURL("", []);
                                window.location.reload();
                            }}>
                                {lB.CANCEL}
                            </Button>
                        </>
                    )}
                </Stack>
            </Container>
        </Popover>
    );
}