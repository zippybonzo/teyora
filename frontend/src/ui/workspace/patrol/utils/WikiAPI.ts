// Static class that handles Wiki API requests
// This EXCLUDES SupaSearch as it is handled separately
// This also only works when in a workspace

import Teyora from "../../../../App";

export default class WikiAPI {
    static async getPageHTML(wikiID: string, pageTitle: string) : Promise<{
        title: string;
        pageid: number;
        text: string;
    }> {
        const response = await fetch(`/api/workspace/wiki/getpagehtml?workspaceID=${
            Teyora.TY.WorkspaceManager.currentWorkspace.meta.teyoraWorkspaceID
        }&wikiID=${wikiID}&pageTitle=${encodeURIComponent(pageTitle)}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        });

        if (!response.ok) throw new Error(response.statusText);

        const responseJSON = await response.json();

        return responseJSON;
    }
}