/*
Tab base class
This is where all tabs are extended from. It contains handlers for setting up and disposing of
persistent states in tabs.

The sidebar and top icons also use this class as it provides a common interface for them
(their ID is modified, see RenderTab.tsx).

The initialPState *MUST* initialise the default values FOR ALL states used by a tab.
If you don't do this then a state won't be created or retrieved properly.
*/

import * as React from "react";
import { createState, State, StateMethodsDestroy } from "@hookstate/core";
import { WithTranslation } from "react-i18next";

enum TabDeveloperWarnings {
    INVALID_TAB_TYPE = "Invalid tab type. TabID is required and you must super to the tab.",
    STATE_VALIDATION_FAILED = "State validation failed",
    INVALID_SIDEBAR_TYPE = "Invalid sidebar type. Should be a component or FALSE if no sidebar",
}

export interface RequiredTabProps extends WithTranslation { // Extending withTranslation for i18n
    tabID: string;
    // eslint-disable-next-line no-unused-vars
    setTPTab?: (current: TPTab<any, any, any>) => void;
}

// Map for storing persistent states
const persistentStates: Map<string, Record<string, State<any>>> = new Map();

// Map for storing if tabs are valid
export const validTabs: string[] = [];

/*
Types:
PStateType: The type of the persistent state. 
PropsType: The type of the props. This must include the tabID prop.
*/
export class TPTab<PStateType, PropsType extends RequiredTabProps, ReactStateType> extends React.Component<PropsType, ReactStateType> {
    // Component flag to stop invalid elements being mounted as tabs.
    // Also makes sure that the extending component calls super(), if not it is invalid
    public __TPVALIDTAB = "notsupered"; 
    public tabID = "";

    constructor(props: PropsType, initialPState: PStateType) {
        super(props); // This supers to react

        const { tabID } = props;
        this.tabID = tabID;

        // We need the tabID to be set
        if (!tabID) throw new Error(TabDeveloperWarnings.INVALID_TAB_TYPE);
        

        // Set up persistent state (create if not present)
        if (!persistentStates.has(tabID)) {
            // Create states for each key in the initial PState
            const states: Record<string, State<any>> = {};
            for (const key in initialPState) {
                states[key] = createState(initialPState[key]);
            }
            persistentStates.set(tabID, states);
        }

        this.__TPVALIDTAB = "yes";
        if (props.setTPTab) props.setTPTab(this); // If requested we can send an instance of ourselves back
    }

    // Get the persistent state (hookstate)
    public getPState(key: string) {
        if (this.__TPVALIDTAB !== "yes") throw new Error(TabDeveloperWarnings.INVALID_TAB_TYPE);
        const { tabID } = this.props;
        if (!persistentStates.has(tabID)) throw new Error(TabDeveloperWarnings.STATE_VALIDATION_FAILED);
        return persistentStates.get(tabID)[key];
    }

    public __disposePState(): void {
        // Dispose of persistent state. Internal use only for when the tab is closed.
        const { tabID } = this;
        if (!persistentStates.has(tabID)) return;
        // For each key in the state, dispose of the state
        const states = persistentStates.get(tabID);
        for (const key in states) {
            // Type casting here is a bit weird but looks like this is how it's done in their docs so
            const state = states[key] as unknown;
            (state as StateMethodsDestroy).destroy();
        }
    }
}