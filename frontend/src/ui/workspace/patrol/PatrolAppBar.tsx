import * as React from "react";
import { AppBar, Box, Toolbar, Typography, Stack, ButtonBase, Tooltip, IconButton, Menu, MenuItem, Badge, Collapse, ListItemIcon, ListItemText } from "@mui/material";
import { TYWordmarkPrimaryContrasted } from "../../components/TYLogo";
import { Lock, AddModerator, ConnectWithoutContact, Delete, Difference, Edit, EditNotifications, Forum, Gavel, Send, Warning, DeleteForever, Visibility, DynamicFeed, Add, Close } from "@mui/icons-material";
import { ContentTab, TeyoraPatrol } from "./TeyoraPatrol";
import { useTranslation } from "react-i18next";
import { TransitionGroup } from "react-transition-group";


export default function PatrolAppBar(props: {
    currentTabs: ContentTab[],
    currentTabIndex: number,
}) {
    const { t } = useTranslation();

    const [isPatrolTabSwitcherOpen, setPatrolTabSwitcherOpen] = React.useState(false);

    const tabSwitcherButtonRef = React.useRef<HTMLButtonElement>(null);

    // Set up animation
    TeyoraPatrol.TP.newTabOpenerAnimation = () => {
        setPatrolTabSwitcherOpen(true);

        // Close after 500ms
        setTimeout(() => {
            setPatrolTabSwitcherOpen(false);
        }, 500);
    };

    let currentTabTitle = t("patrol:header.noTabTitle");
    let currentTabSubtitle = t("patrol:header.noTabSubtitle");
    if (props.currentTabIndex >= 0 && props.currentTabIndex < props.currentTabs.length) {
        currentTabTitle = props.currentTabs[props.currentTabIndex].title;
        currentTabSubtitle = props.currentTabs[props.currentTabIndex].subtitle;
    }

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar
                position="fixed"
                enableColorOnDark
                sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }} // Make sure the appbar is on top of the drawer
            >
                <Toolbar>
                    {/* This needs to be restructured, maybe with a flexbox */}
                    <Stack spacing={3} direction={"row"}>
                        {/* Logo (click toggles Feed drawer) */}
                        <Tooltip title={t("patrol:feeddrawer.toggleButton")}>
                            <ButtonBase sx={{
                                display: "flex",
                                alignItems: "center",
                                width: "auto",
                            }}
                            onClick={() => {
                                TeyoraPatrol.TP.toggleFeedDrawer();
                            }}
                            ref={tabSwitcherButtonRef}
                            >
                                <TYWordmarkPrimaryContrasted style={{
                                    height: "35px",
                                    width: "auto",
                                }} />
                            </ButtonBase>
                        </Tooltip>

                        {/* Tab switcher */}
                        <Box sx={{
                            display: "flex",
                            alignItems: "center",
                        }}>

                            <Tooltip title={t("patrol:header.openTabSwitcher")}>
                                <ButtonBase onClick={() => {
                                    setPatrolTabSwitcherOpen(!isPatrolTabSwitcherOpen); // Toggle menu
                                }}
                                >
                                    {/* Badge: -1 to ignore homepage in count */}
                                    <Badge badgeContent={props.currentTabs.length - 1} color={
                                        props.currentTabs.length > 50 ? "error" : "secondary"
                                    } anchorOrigin={{
                                        vertical: "bottom",
                                        horizontal: "right",
                                    }}>
                                        <DynamicFeed />
                                    </Badge>
                                    <Stack maxWidth={"20vw"} textAlign={"left"} sx={{
                                        paddingLeft: "1.5rem",
                                    }}>
                                        <Typography variant="h6" noWrap>
                                            {currentTabTitle}
                                        </Typography>
                                        <Typography variant="caption" noWrap>
                                            {currentTabSubtitle}
                                        </Typography>
                                    </Stack>
                                </ButtonBase>
                            </Tooltip>
                            <Menu anchorEl={tabSwitcherButtonRef.current} open={isPatrolTabSwitcherOpen} onClose={() => {
                                setPatrolTabSwitcherOpen(false);
                            }}
                            >


                                <TransitionGroup>
                                    {props.currentTabs.map((tab, index) => {
                                        if (index === 0) return null; // Don't list the homepage
                                        return (
                                            <Collapse key={index} in={true}>
                                                <MenuItem key={index} onClick={() => {
                                                    // TODO: add closing preference on change
                                                    TeyoraPatrol.TP.setTabIndex(index);
                                                }}>
                                                    <Stack width={"20vw"} textAlign={"left"}>
                                                        {index === props.currentTabIndex && (
                                                            <Typography variant="caption" noWrap sx={{
                                                                fontStyle: "all-small-caps",
                                                                color: "success.dark",
                                                            }}>
                                                                {t("patrol:header.currentTab")}
                                                            </Typography>
                                                        )}
                                                        <Typography variant="h6" noWrap>
                                                            {tab.title}
                                                        </Typography>
                                                        <Typography variant="caption" noWrap>
                                                            {tab.subtitle}
                                                        </Typography>
                                                    </Stack>
                                                    {/* Close tab iconbutton (not on homepage */}
                                                    {index !== 0 && <IconButton onClick={() => {
                                                        TeyoraPatrol.TP.closeTab(index);
                                                    }
                                                    }>
                                                        <Close />
                                                    </IconButton>}
                                                </MenuItem>
                                            </Collapse>
                                        );
                                    })}
                                </TransitionGroup>

                                {/* New tab menu item (opens search/nav tool) */}
                                <MenuItem onClick={()=>{
                                    TeyoraPatrol.TP.openSupaSearch();
                                }}>
                                    <ListItemIcon>
                                        <Add fontSize="small" />
                                    </ListItemIcon>
                                    <ListItemText>
                                        {t("patrol:header.newTab")}
                                    </ListItemText>
                                </MenuItem>
                                
                            </Menu>

                        </Box>

                        {/* Action icons */}
                        <Box sx={{
                            display: "flex",
                            alignItems: "center",
                            color: "primary.contrastText",
                        }}>

                            {props.currentTabs[props.currentTabIndex].topControls || []}

                        </Box>

                        {/* Lastly...
                        <Box sx={{
                            float: "right",
                        }}>
                            <AppbarUserMenu />
                        </Box> */}
                    </Stack>
                </Toolbar>
            </AppBar>
        </Box>
    );

}
