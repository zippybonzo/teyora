import { Box, Button, Container, Stack, Typography } from "@mui/material";
import * as React from "react";
import { useTranslation } from "react-i18next";
import Feed from "./Feed";
import Teyora from "../../../../App";

export default function FeedDrawerContent() {
    const { t } = useTranslation();

    return <Box sx={{
        overflow: "auto",
    }}>
        
        {/* Feeds */}
        {Teyora.TY.WorkspaceManager.currentWorkspace.feeds.length < 1 ? (
            // Workspace contains no feeds
            <Container sx={{
                textAlign: "center",
                alignItems: "center",
                paddingTop: "2rem",
            }}>
                <Stack alignContent={"center"} spacing={1}>
                    <Typography variant="h6">
                        {t("patrol:feeddrawer.empty.title")}
                    </Typography>
                    <Typography variant="body1">
                        {t("patrol:feeddrawer.empty.description")}
                    </Typography>
                    <Button variant="outlined" color="primary">
                        {t("patrol:feeddrawer.empty.openFeedPreferences")}
                    </Button>
                </Stack>
            </Container>
        ) :
        
            ( // Todo, list feeds here and arange them properly
                <Stack spacing={1}>
                    {
                        Teyora.TY.WorkspaceManager.currentWorkspace.feeds.map(feed => (
                            <Feed key={feed.id} {...feed} />
                        ))
                    }
                </Stack>
            )
        }
    </Box>;
}
