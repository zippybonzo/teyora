import { createTheme, Theme as MUITheme, ThemeOptions } from "@mui/material/styles";
import Teyora from "../App";

export const TeyoraTheme: ThemeOptions[] = [
    /*
    YellowxGrey Teyora themes (default) - see https://material.io/resources/color 
    Index 0 = Light
    Index 1 = Dark
    */
    {

        palette: {
            mode: "light",
            primary: { // Yellow
                light: "#fffd61",
                main: "#ffca28",
                dark: "#c79a00",
                contrastText: "#000"
            },
            secondary: { // Blue-grey
                light: "#718792",
                main: "#455a64",
                dark: "#1c313a",
                contrastText: "#fff"
            }
        }
    },

    // Dark theme
    {
        palette: {
            mode: "dark",
            primary: {
                main: "#ffc107",
            },
            secondary: {
                main: "#424242",
            },
            background: {
                default: "#212121",
            },
        }
    }
];

export const MonotoneTheme: ThemeOptions[] = [
    // Monotone themes (basically all black/all white)
    {
        palette: {
            mode: "light",
            primary: {
                light: "#000000",
                main: "#000000",
                dark: "#000000",
                contrastText: "#ffffff"
            },
            secondary: {
                light: "#ffffff",
                main: "#ffffff",
                dark: "#ffffff",
                contrastText: "#000000"
            }
        },

        typography: {
            fontFamily: "Times New Roman",
        }
    },
    {
        palette: {
            mode: "dark",
            primary: {
                light: "#ffffff",
                main: "#ffffff",
                dark: "#ffffff",
                contrastText: "#000000"
            },
            secondary: {
                light: "#000000",
                main: "#000000",
                dark: "#000000",
                contrastText: "#ffffff"
            }
        },
        typography: {
            fontFamily: "Times New Roman",
        }
    }
];

export const RedWarningTheme: ThemeOptions[] = [
    // Red-warning themes
    {
        palette: {
            mode: "light",
            primary: {
                main: "#d32f2f",
            },
            secondary: {
                main: "#ad1457",
            },
            background: {
                default: "##ffcdd2",
            },
        }
    },
    {
        palette: {
            mode: "dark",
            primary: {
                main: "#d32f2f",
            },
            secondary: {
                main: "#ad1457",
            },
            background: {
                default: "#210b0b",
            },
        }
    }
];



// Sets the theme to light or dark based on what the browser prefers
// We also provide preview options here to allow users to preview themes before applying them
export const UserTheme = (previewOptions?: {
    userPreviewThemePrefKey: string | null,
    userPreviewDarkModePref: string | null,
}): ThemeOptions => {
    const { PreferencesManager } = Teyora.TY;
    const userThemePrefKey: string = previewOptions?.userPreviewThemePrefKey
        || PreferencesManager.getPreference("appearance/theme", "teyora");
    const userDarkModePref = previewOptions?.userPreviewDarkModePref
        || PreferencesManager.getPreference("appearance/themeVariant", "system");

    const systemPrefersDark = window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches;

    const UserThemePref: ThemeOptions[] = ({
        "teyora": TeyoraTheme,
        "monotone": MonotoneTheme,
        "redwarning": RedWarningTheme
    })[userThemePrefKey] || TeyoraTheme;

    if (userDarkModePref === "system") {
        return systemPrefersDark ? UserThemePref[1] : UserThemePref[0];
    } else {
        return userDarkModePref === "dark" ? UserThemePref[1] : UserThemePref[0];
    }
};

// Todo: Swap for user preference for this workspace
export function getTheme(theme: ThemeOptions): MUITheme {
    return createTheme(theme);
} 