/*
    The integrity manager prevents the user or extensions from modifying
    user privileges on the user side, mainly for non-approved users.

    This also checks if the users session is still valid (every minute or so).
    This keeps our "online status" indicator up to date.
*/

import Teyora from "../App";
import * as React from "react";
import i18n from "../i18n";
import TeyoraUI from "../ui/TeyoraUI";
import { LocalisedButtons } from "../ui/LocalisedButtons";

interface iPrivileges {
    isAdmin: boolean;
    isSuspended: boolean;
    isVerified: boolean;
    isApproved: boolean;
    isLocked: boolean;
    isOverlord: boolean;
}

export default class IntegrityManager {
    private expectedPrivileges: iPrivileges;

    private async tickProtection() {
        // Check if current user privileges have changed
        const currentPrivileges = {
            isAdmin: Teyora.TY.CurrentUser.isAdmin,
            isSuspended: Teyora.TY.CurrentUser.isSuspended,
            isVerified: Teyora.TY.CurrentUser.isVerified,
            isApproved: Teyora.TY.CurrentUser.isApproved,
            isLocked: Teyora.TY.CurrentUser.isLocked,
            isOverlord: Teyora.TY.CurrentUser.isOverlord
        };

        // If the privileges have changed, stop everything
        if (JSON.stringify(currentPrivileges) !== JSON.stringify(this.expectedPrivileges)) {
            const { t } = i18n;

            // eslint-disable-next-line no-inner-declarations
            function Crasher(): JSX.Element {
                throw new Error(t("errors:im.checkFail"));
            }

            Teyora.TY.setState({
                activeWindow: (
                    <Crasher />
                )
            });


        }
    }

    private async checkSessionTick() {
        const { t } = i18n;
        const lB = LocalisedButtons();
        // Check if the session is still valid by making a request to get current user
        try {
            const thisUser = await Teyora.TY.authManager.getUser(true); // true so we don't redirect
            if (thisUser == null) {
                throw new Error("User not found");
            }

            // If the user is not the same as the current user, reload the page
            if (thisUser.userID !== Teyora.TY.CurrentUser.userID) {
                await TeyoraUI.showAlertBox(t("errors:im.otherUserLoggedIn"));
                window.location.reload();
                return;
            }

            // If the user has been suspended reload the page
            if (thisUser.isSuspended) {
                Teyora.TY.setState({
                    activeWindow: (
                        <h1>{t("errors:im.suspended")}</h1>
                    )
                });
                window.location.reload();
                return;
            }

            // If any other rights have changed warn the user
            if (thisUser.isAdmin !== Teyora.TY.CurrentUser.isAdmin ||
                thisUser.isVerified !== Teyora.TY.CurrentUser.isVerified ||
                thisUser.isApproved !== Teyora.TY.CurrentUser.isApproved ||
                thisUser.isLocked !== Teyora.TY.CurrentUser.isLocked ||
                thisUser.isOverlord !== Teyora.TY.CurrentUser.isOverlord) {
                await TeyoraUI.showAlertBox(t("errors:im.privilegesChanged"));
                return;
            }

            // All checks passed, we can rerun the tick
            setTimeout(() => this.checkSessionTick(), 60000);
        } catch (error) {
            // Warn the user if the session is invalid and get them to sign in again

            const result = await TeyoraUI.showAlertBox(t("errors:im.sessionInvalid.message"), t("errors:im.sessionInvalid.title"), [
                lB.LOGIN,
                lB.RETRY
            ]);

            if (result === lB.LOGIN) {
                // Open the reauthorisation endpoint (/api/oauth/wmf/redirect?after=closetab)
                // in a new window
                window.open("/api/oauth/wmf/redirect?after=closetab", "_blank");
                return;
            }

            // Retry the check after 5 seconds
            setTimeout(() => this.checkSessionTick(), 5000);
        }
    }

    constructor() {
        if (Teyora.TY.CurrentUser == null) return; // Don't run if not logged in
        if (Teyora.TY.CurrentUser.isSuspended) return; // Don't run if suspended

        // Set the expected privileges based on the current user
        this.expectedPrivileges = {
            isAdmin: Teyora.TY.CurrentUser.isAdmin,
            isSuspended: Teyora.TY.CurrentUser.isSuspended,
            isVerified: Teyora.TY.CurrentUser.isVerified,
            isApproved: Teyora.TY.CurrentUser.isApproved,
            isLocked: Teyora.TY.CurrentUser.isLocked,
            isOverlord: Teyora.TY.CurrentUser.isOverlord
        };

        setInterval(() => this.tickProtection(), 5000);
        setTimeout(() => this.checkSessionTick(), 30000);
    }
}