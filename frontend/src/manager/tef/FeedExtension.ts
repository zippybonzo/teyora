// Loads and handles feed extensions
// Feed extensions are modules
// See also FeedExtension.mjs for the implementation

import Teyora from "../../App";
import TeyoraUI from "../../ui/TeyoraUI";
import FeedEdit from "../../ui/workspace/patrol/feeddrawer/FeedEdit";
import { TeyoraPatrol } from "../../ui/workspace/patrol/TeyoraPatrol";
import i18n from "../../i18n";
import * as MUI from "@mui/material"; // For UI components

export interface iFeedExtension {
    // Passed through args
    Teyora: Teyora;
    TeyoraUI: TeyoraUI;
    TeyoraPatrol: TeyoraPatrol;
    MUI: typeof MUI;
    ExtensionFail: (message: string) => void;

    // Extension specific
    ActiveWikis: string[];
    NewFeedEdit: (edit: FeedEdit) => void;

    init: () => Promise<void>;
}

export default class FeedExtension {
    private feedThrottlingOn:boolean = Teyora.TY.PreferencesManager.getPreference(
        "performance/feedThrottling",
        "enabled",
        true
    ) == "enabled";

    private feedThrottlingMin: number = Teyora.TY.PreferencesManager.getPreference(
        "performance/feedThrottlingDelayMin", 300, true
    );

    private feedThrottlingMax: number = Teyora.TY.PreferencesManager.getPreference(
        "performance/feedThrottlingDelayMax", 1000
    );

    public extensionID: string;
    public extensionName: string;
    public activeWikis: string[] = [];
    public associatedFeeds: string[] = [];
    public extension: iFeedExtension;
    public hasInit = false;
    public extensionFailListeners: (() => void)[] = [];
    public extensionMessageListeners: (() => void)[] = []; // Called when new message/warning/info etc.
    public newFeedEditListeners: ({
        wikis: string[],
        call: (edit: FeedEdit) => void
    })[] = [];

    private enquedEdits: FeedEdit[] = [];

    constructor(extensionID: string, extensionName: string) {
        this.extensionID = extensionID;
        this.extensionName = extensionName;
    }

    public async init() {
        this.hasInit = true;

        const { t } = i18n;
        // TODO: Impliment caching
        // Import extension class
        const extensionModule = await import(/* webpackIgnore: true */ `/extensions/${this.extensionID}.mjs`);
        // Create a new extension instance
        // Requires Teyora, TeyoraUI, TeyoraPatrol
        this.extension = new extensionModule.default({
            Teyora,
            TeyoraUI,
            TeyoraPatrol,
            MUI,
            ActiveWikis: this.activeWikis,
        });

        this.extension.ExtensionFail = (message: string) => {
            TeyoraUI.showSnackbar(t("errors:extensionfail", { extension: this.extensionName, message }), {
                variant: "error",
                title: t("errors:extensionfailTitle"),
            });

            this.extensionFailListeners.forEach(listener => listener());
        };

        this.extension.NewFeedEdit = (edit: FeedEdit) => {
            // console.log("New edit received");
            // Push if feed is not throttled
            if (!this.feedThrottlingOn) this.releaseEdit(edit);

            // Add to enqued edits
            this.enquedEdits.push(edit);
        };



        await this.extension.init();

        if (this.feedThrottlingOn) setTimeout(() => this.releaseEditTick(), this.feedThrottlingMin);
    }

    // Returns an unsubscribe function
    public subscribe(wikis: string[], callback: (edit:FeedEdit) => void): () => void {
        // Add wikis to active wikis if not already added
        wikis.forEach(wiki => {
            if (!this.activeWikis.includes(wiki)) this.activeWikis.push(wiki);
        });

        this.extension.ActiveWikis = this.activeWikis;

        // Add listener
        this.newFeedEditListeners.push({
            wikis,
            call: callback
        });

        // Return unsubscribe function
        return () => {
            // Remove wikis from active wikis
            wikis.forEach(wiki => {
                const index = this.activeWikis.indexOf(wiki);
                if (index > -1) this.activeWikis.splice(index, 1);
            });

            // Remove listener
            this.newFeedEditListeners.splice(this.newFeedEditListeners.findIndex(listener => listener.wikis.every(wiki => wikis.includes(wiki))), 1);

            // Update active wikis
            this.extension.ActiveWikis = this.activeWikis;
        };
    }

    private releaseEdit(edit: FeedEdit) {
        // console.log("Release edit request");
        // Release an edit to all listeners
        this.newFeedEditListeners.forEach(listener => {
            if (listener.wikis.includes(edit.wikiId)) listener.call(edit);
        });
    }

    private async releaseEditTick() {
        // Run on tick, which is determined
        if (this.enquedEdits.length === 0) {
            setTimeout(() => this.releaseEditTick(), this.feedThrottlingMax);
            return;
        }

        // Release the oldest edit
        const edit = this.enquedEdits.shift();
        
        // Release edit
        this.releaseEdit(edit);

        // Calculate the delay based on max and min where the max is used when
        // the enqued edit length is 0 and the min is used when the enqued edit
        // length is over 50
        const delay = this.feedThrottlingMax - (this.feedThrottlingMax - this.feedThrottlingMin) * (this.enquedEdits.length / 50);
        
        const wait = Math.max(delay, this.feedThrottlingMin); // Wait at least the min

        // console.log(
        //     `Feed throttling info:
        //     Enqued edits: ${this.enquedEdits.length}
        //     Delay: ${wait} ${wait === this.feedThrottlingMin ? "CAPPED at min" : "not capped"}`
        // );

        setTimeout(() => this.releaseEditTick(), wait);
    }
}