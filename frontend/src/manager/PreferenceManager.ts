import TeyoraUI from "../ui/TeyoraUI";
import i18n from "../i18n";
import { LocalisedButtons } from "../ui/LocalisedButtons";
import Teyora from "../App";

export default class PreferenceManager {
    // Gets and sets preferences for the current user
    // Private to stop people from setting the values directly which may cause bugs
    private PreferenceValues: { [key: string]: any } = {}; 

    public PreferenceChangesQueue: { [key: string]: any } = {};

    public async refreshPreferences(): Promise<void> {
        // Fetch /api/user/preferences
        // Set the values
        try {
            await fetch("/api/user/preferences").then(async response => {
                if (response.status !== 200) {
                    throw new Error("Failed to fetch preferences (server error?)");
                }
                this.PreferenceValues = await response.json();
            });
        } catch (error) {
            console.log(error);
            // Commented out as this may cause bugs on load
            // const { t } = i18n;
            // const lB = LocalisedButtons();
            // // Inform the user that the preferences couldn't be fetched and ask them to refresh
            // // or log out and back in again
            // console.error(error);
            // const errorDialogResult = await TeyoraUI.showAlertBox(t("ui:loadPreferencesError.message"), t("ui:loadPreferencesError.title"), [
            //     lB.LOGOUT,
            //     lB.RELOAD
            // ]);

            // if (errorDialogResult === lB.LOGOUT) {
            //     // Logout 
            //     TeyoraUI.confirmLogout();
            // } else {
            //     // Refresh the page
            //     window.location.reload();
            // }
        }
    }

    public getPreference(
        key: string,
        defaultValue: any,
        useDefaultIfUserNotApproved = false,
        useValueIfUserNotApproved: any = "not used here"
    ): any {
        // If the user isn't approved, use the default value
        if (useValueIfUserNotApproved != "not used here" && !Teyora.TY.CurrentUser.isApproved)
            return useValueIfUserNotApproved;
        
        if (useDefaultIfUserNotApproved && !Teyora.TY.CurrentUser.isApproved)
            return defaultValue;
        
        return this.PreferenceValues[key] || defaultValue;
    }

    // This QUEUES a preference change, but doesn't set it until commitPreferences is called
    public setPreference(key: string, value: any) {
        this.PreferenceChangesQueue[key] = value;
    }

    public clearSetQueue() {
        this.PreferenceChangesQueue = {};
    }

    public async commitPreferences(): Promise<void> {
        // It'd send the changes to the server from the queue
        // Merge the changes into the current values
        const newPref = { ...this.PreferenceValues, ...this.PreferenceChangesQueue };
        

        // Send the changes to the server
        try {
            const fR = await fetch("/api/user/preferences", {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(newPref)
            });

            if (fR.status !== 200) {
                throw new Error("Failed to save preferences (server error?)");
            }

            // Update the values
            this.PreferenceValues = newPref;
            this.PreferenceChangesQueue = {};

        } catch (error) {
            const { t } = i18n;
            const lB = LocalisedButtons();
            // Inform the user that the preferences couldn't be fetched and ask them to refresh
            // or log out and back in again
            console.error(error);
            await TeyoraUI.showAlertBox(t("ui:savePreferencesError.message"), t("ui:savePreferencesError.title"), [
                lB.OK
            ]);
            throw new Error("Failed to save preferences"); // Stop the promise from resolving
        }
    }

}