/* eslint-disable */
const path = require("path");
const ReactRefreshTypeScript = require('react-refresh-typescript');
const isDevelopment = process.env.NODE_ENV !== 'production';

module.exports = {
    mode: isDevelopment ? 'development' : 'production',
    devServer: {
        compress: true,
        hot: true,
        port: 45991,
        client: {
            progress: true,
        },
        static: {
            directory: path.join(__dirname, '../static'),
        },
        proxy: {
            '/api': 'http://localhost:45990',
        },
    },
    entry: ["./src/App.tsx"],
    output: {
        path: path.resolve(__dirname, "../static"),
        filename: "Teyora.js"
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    {
                      loader: require.resolve('ts-loader'),
                      options: {
                        getCustomTransformers: () => ({
                          before: [isDevelopment && ReactRefreshTypeScript()].filter(Boolean),
                        }),
                        transpileOnly: isDevelopment,
                      },
                    },
                  ],
                exclude: [
                    path.resolve(__dirname, "node_modules")
                ]
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    }
};