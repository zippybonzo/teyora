import { MangoResponse } from "nano";
import { WorkspaceEntry } from "../db/dbcollections/workspace";
import TeyoraDB from "../db/TeyoraDB";

export interface WorkspaceMeta {
    teyoraWorkspaceID: string;
    workspaceOwner: string;
    workspaceName: string;
    workspaceDescription: string;
    workspaceParticipants: string[];
    workspaceLastActive: Date;
    workspaceIsLocked: boolean;
    workspaceIsPublic: boolean;
    workspaceIsDeleted: boolean;
}

export default class WorkspaceHelper {
    public static async createWorkspace(
        db: TeyoraDB,
        workspaceName: string,
        workspaceDescription: string,
        workspaceOwner: string,
    ) : Promise<string> {
        // Get the users current workspace count
        const WorkspaceMetas = await WorkspaceHelper.getWorkspaces(db, workspaceOwner);

        // Filter workspaces that are deleted and aren't owned by the user
        const WorkspacesCount: WorkspaceMeta[] = WorkspaceMetas.filter((meta) => {
            return !meta.workspaceIsDeleted && meta.workspaceOwner === workspaceOwner;
        });

        if (WorkspacesCount.length >= 100) {
            throw new Error("You have reached the maximum number of workspaces");
        }


        // Verify that the workspace name is not longer than 50 characters or shorter then 10 characters
        if (workspaceName.length > 50 || workspaceName.length < 10) {
            throw new Error("Workspace name must be between 10 and 50 characters long");
        }

        // Verify that the workspace description is not longer than 1500 characters or shorter then 10 characters
        if (workspaceDescription.length > 1500 || workspaceDescription.length < 10) {
            throw new Error("Workspace description must be between 10 and 1500 characters long");
        }

        // Generate a random workspace ID
        const workspaceID = Math.random().toString(36).substring(2, 15);

        await db.Workspace.insert(new WorkspaceEntry(
            workspaceID,
            workspaceOwner,
            workspaceName,
            workspaceDescription,
            [ workspaceOwner ],
            {},
            new Date(),
            new Date(),
            new Date(),
            // By default make the workspace only for the owner
            false,
            false,
            false,
            false,
        ));

        return workspaceID;
    }

    public static async getWorkspaces(db: TeyoraDB, userID: string) : Promise<WorkspaceMeta[]> {
        // This may be a bit expensive, so maybe rework this later

        const WorkspaceEntries: MangoResponse<WorkspaceEntry>  = await db.Workspace.find({
            "fields": [
                "teyoraWorkspaceID",
                "workspaceName",
                "workspaceDescription",
                "workspaceOwner",
                "workspaceParticipants",
                "workspaceIsLocked",
                "workspaceIsPublic",
                "workspaceIsDeleted",
                "workspaceLastActive",
            ],

            "selector": {
                "workspaceParticipants": {
                    "$elemMatch": {
                        "$eq": userID
                    },
                },

                "workspaceOwner": userID,
            },
        });

        // Convert workspace entry docs to workspace entries
        const metas: WorkspaceMeta[] = [];

        for (const entry of WorkspaceEntries.docs) {
            metas.push({
                teyoraWorkspaceID: entry.teyoraWorkspaceID,
                workspaceOwner: entry.workspaceOwner,
                workspaceName: entry.workspaceName,
                workspaceDescription: entry.workspaceDescription,
                workspaceParticipants: entry.workspaceParticipants,
                workspaceLastActive: entry.workspaceLastActive,
                workspaceIsLocked: entry.workspaceIsLocked,
                workspaceIsPublic: entry.workspaceIsPublic,
                workspaceIsDeleted: entry.workspaceIsDeleted,
            });
        }

        return metas;
    }

    /*
        Get workspace from DB - here we already only check if the user has access to the workspace
        otherwise, no results to the query means that it doesn't exist to the user
        Even for admins they need to let themselves in to the workspace FIRST and
        can't just find it by ID
    */
    public static async getWorkspace(db: TeyoraDB, workspaceID: string, userID: string) : Promise<WorkspaceEntry> {
        const WorkspaceEntry: MangoResponse<WorkspaceEntry> = await db.Workspace.find({
            "selector": {
                "teyoraWorkspaceID": workspaceID,
                "workspaceParticipants": {
                    "$elemMatch": {
                        "$eq": userID
                    },
                },

                "workspaceOwner": userID,
            },
        });

        if (WorkspaceEntry.docs.length === 0) {
            throw new Error("Workspace not found under this user");
        }

        return WorkspaceEntry.docs[0];
    }
}