import Nano from "nano";
export interface iProfileEntry extends Nano.MaybeDocument {
    username: string, // The username of the user, used by those who don't know UID i.e. (WMF:Ed6767)
    userID: string, // The userID of the user, used to identify the user
    profile: any // The profile of the user, e.g. their name, pronouns, bio, etc.
}

export interface UserProfile { // This is only observed if the editor is approved
    nickname?: string, // The nickname of the user, if not set
    bio?: string, // The bio of the user, if not set
    profilePicture?: string, // The profile picture of the user, if not set
}


export class ProfileEntry implements iProfileEntry {
    _id?: string;
    _rev?: string;
    username: string;
    userID: string;
    profile: UserProfile;
    
    constructor(
        username: string,
        userID: string,
        profile: UserProfile
    ) {
        this._id = undefined;
        this._rev = undefined;
        this.username = username;
        this.userID = userID;
        this.profile = profile;
    }
    
    processAPIResponse(response: Nano.DocumentInsertResponse) {
        if (response.ok === true) {
            this._id = response.id;
            this._rev = response.rev;
        }
    }
}