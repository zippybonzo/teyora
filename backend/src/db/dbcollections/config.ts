import Nano from "nano";
export interface iConfigEntry extends Nano.MaybeDocument {
    userID: string, // The userID of the user, used to identify the user here as configs are not public
    config: any, // The config object
    lastModified: Date, // The last time the config was modified
    previousConfig: any, // The previous config object (for undoing)
}

export class ConfigEntry implements iConfigEntry {
    _id?: string;
    _rev?: string;
    userID: string;
    config: any;
    lastModified: Date;
    previousConfig: any;
    
    constructor(
        userID: string,
        config: any,
        lastModified: Date,
        previousConfig: any
    ) {
        this._id = undefined;
        this._rev = undefined;
        this.userID = userID;
        this.config = config;
        this.lastModified = lastModified;
        this.previousConfig = previousConfig;
    }
    
    processAPIResponse(response: Nano.DocumentInsertResponse) {
        if (response.ok === true) {
            this._id = response.id;
            this._rev = response.rev;
        }
    }
}