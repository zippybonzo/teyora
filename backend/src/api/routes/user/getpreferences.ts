import UserAuth from "../../../helpers/UserAuth";
import AuthedRoute, { AuthedRouteExit } from "../../AuthedRoute";
import { RouteParams } from "../RouteDefs";

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: false, // Users must be unlocked before prefs can be set or retrieved
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const preferences = await UserAuth.getUserPreferencesNPV(r.db, r.CRI.user);
            r.res.status(200).json(preferences);
        },
    });
    route.run(props.req, props.res);
};