import UserAuth from "../../../helpers/UserAuth";
import AuthedRoute, { AuthedRouteExit } from "../../AuthedRoute";
import { RouteParams } from "../RouteDefs";

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: false, // Users must be unlocked before prefs can be set or retrieved
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const { req, res, db, CRI } = r;
            // Ensure the body is less than 512KB
            if (req.body.length > 512 * 1024) {
                res.status(400).json({
                    error: "Bad Request",
                    message: "The preferences are too large. Please try again with a smaller file."
                });
                return;
            }

            // Update the user's preferences
            const preferences = await UserAuth.setUserPreferencesNPV(db, CRI.user, req.body);
            res.status(200).json(preferences);
        },
    });
    route.run(props.req, props.res);
};