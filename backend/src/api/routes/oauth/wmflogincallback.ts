import JWTVerify from "../../../helpers/JWTVerify";
import UserAuth from "../../../helpers/UserAuth";
import APIRouter from "../../APIRouter";
import { OAuthTokens } from "../../WMFOAuthDriver";
import { RouteParams } from "../RouteDefs";

export default async (props: RouteParams) => {
    const { res, req, db } = props;
    
    if (!req.query.oauth_verifier || !req.query.oauth_token || !req.cookies.TEMPOAuthSecret) {
        res.status(400).json({
            message: "No oauth_verifier or oauth_token provided, or cookie missing",
        });
        return;
    }

    const token: OAuthTokens = await APIRouter.wmfOAuth.getToken(
        req.query.oauth_verifier.toString(),
        req.cookies.TEMPOAuthToken,
        req.cookies.TEMPOAuthSecret
    );

    // Clear the cookie
    res.clearCookie("TEMPOAuthSecret");
    res.clearCookie("TEMPOAuthToken");

    if (token.error) {
        res.status(500).json({
            message: "Something went wrong with your login request. Please try again.",
        });
        return;
    }

    // Get metawiki profile
    const authxios = APIRouter.wmfOAuth.getAuthedAxios(token);
    let profileRequest;
    try {
        profileRequest = (await authxios.get("https://meta.wikimedia.org/w/api.php?action=query&meta=userinfo&format=json")).data;
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "Failed to get user info from WMF OAuth",
        });
        return;
    } 
    

    const wmfUsername = profileRequest.query.userinfo.name;

    // Get the user info
    let userInfo = await UserAuth.getUserNPVByUsername(db,`WMF:${wmfUsername}`);

    // If the user doesn't exist, create it
    if (!userInfo) {
        userInfo = await UserAuth.createUser(db, `WMF:${wmfUsername}`, "Teyora WMF Callback API", "WMF");
    } else {
        // The user does exist, so to prevent account hijacking we have to check if this account was renamed prior to
        // the creation of the user of Teyora. If it was, we cannot allow the user to login for security reasons.

        // Make a request to the WMF API to get rename logs
        let renameLogs;
        try {
            renameLogs = (await authxios.get(`https://meta.wikimedia.org/w/api.php?action=query&list=logevents&letype=renameuser&lelimit=1&letitle=User:${wmfUsername}&format=json`)).data.query;
        
            if (renameLogs.logevents.length > 0 && renameLogs.logevents[0].params.olduser == wmfUsername) {
                // Compare the action timestamp to the creation timestamp of the user
                const creationTimestamp = new Date(userInfo.creationDate);
                const actionTimestamp = new Date(renameLogs.logevents[0].timestamp);
                if (actionTimestamp > creationTimestamp) {
                    // The user has been renamed since the creation of the user of Teyora, so we cannot allow the user to login
                    // Redirect to the issue page
                    res.redirect("/?error=login:siteNotices.renamedAccountIssue");
                    return;
                }
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({
                message: "Failed to get rename logs from WMF OAuth",
            });
            return;
        }
    }
        
    // Make the JWT
    const jwtToken = await JWTVerify.newJWT(db, userInfo, token);

    // Set a new HttpOnly cookie
    res.cookie("DONOTSHARE_teyoraJWT", jwtToken, {
        httpOnly: true, // We don't set a domain as by default it's the current domain
    });

    // If the user has a "after" cookie, delete it.
    // ONLY handle the after cookie if it has a valid action (add more here).
    // DO NOT add user inputted URL redirection as this may cause a security issue.
    if (req.cookies.TEMPOAuthAfter) {
        const after = req.cookies.TEMPOAuthAfter;
        res.clearCookie("TEMPOAuthAfter");
        if (after === "closetab") {
            res.send(`
                <!-- After: ${after} -->
                You're logged in to Teyora.
                You can now return to your previous session, or <a href='/'>click here</a> to start a new one.
                <script>window.close();</script>`); // Script will only work if opened properly
            return;
        }
    }
        
    // Head to the frontend
    res.redirect("/");
        
    return;
};