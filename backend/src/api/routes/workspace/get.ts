import { WorkspaceEntry } from "../../../db/dbcollections/workspace";
import WorkspaceHelper from "../../../helpers/WorkspaceHelper";
import AuthedRoute, { AuthedRouteExit } from "../../AuthedRoute";
import { RouteParams } from "../RouteDefs";

// Gets workspace configuration

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: false,
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const { db, CRI } = r;
            const { workspaceID } = r.req.query;
            if (!workspaceID) {
                r.res.status(400).json({
                    error: "No workspace ID provided",
                });
                return;
            }

            try {
                const workspace:WorkspaceEntry = await WorkspaceHelper.getWorkspace(
                    db,
                    workspaceID as string,
                    CRI.user.userID
                );

                if (workspace.workspaceIsLocked) {
                    r.res.status(403).json({
                        error: "Workspace is locked",
                    });
                    return;
                }

                r.res.status(200).json({
                    workspace,
                });
            } catch (error) {
                r.res.status(404).json({
                    error: "Workspace not found",
                });
            }
        },
    });
    route.run(props.req, props.res);
};