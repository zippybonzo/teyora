import { WorkspaceEntry } from "../../../../db/dbcollections/workspace";
import WorkspaceHelper from "../../../../helpers/WorkspaceHelper";
import { WikiSupport } from "../../../../info/wikis/WikiSupport";
import APIRouter from "../../../APIRouter";
import AuthedRoute, { AuthedRouteExit } from "../../../AuthedRoute";
import { RouteParams } from "../../RouteDefs";

/**
 * Get page HTML
 * Grabs the HTML from a page on a wiki
 * The Wiki *MUST* be in the workspace
 * Todo later: Add support for revIDs instead of page names
 */


export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: false,
            SuspendedCanRun: false,
            LockedUsersCanRun: false,
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const { db, CRI } = r;
            const { workspaceID, pageTitle, wikiID } = r.req.query;
            if (!workspaceID || !pageTitle || !wikiID) {
                r.res.status(400).json({
                    error: "Missing one of the following: workspaceID, pageTitle, wikiID",
                });
                return;
            }

            // Limit the workspace ID length to prevent DoS
            if (workspaceID.length > 24 || pageTitle.length > 250 || wikiID.length > 20) {
                r.res.status(403).json({
                    error: "Operation not permitted",
                });
                return;
            }

            try {

                // Let's grab the workspace
                const workspace: WorkspaceEntry = await WorkspaceHelper.getWorkspace(
                    db,
                    workspaceID as string,
                    CRI.user.userID
                );

                if (workspace.workspaceIsLocked) {
                    r.res.status(403).json({
                        error: "Workspace is locked",
                    });
                    return;
                }

                // Okay, now let's grab all the Wiki IDs from the workspace configuration
                const wikiIDs: string[] = workspace.workspaceConfig.wikis;

                // If wikiIDs is empty, we can't do anything
                if (!wikiIDs || wikiIDs.length === 0) {
                    r.res.status(400).json({
                        error: "No wikis in workspace",
                    });
                    return;
                }

                // Enforce the limit of max 15 wikis to each workspace
                if (wikiIDs.length > 15) {
                    r.res.status(400).json({
                        error: "Too many wikis in workspace",
                    });
                    return;
                }

                // If the wiki is not in the workspace, we can't do anything
                if (!wikiIDs.includes(wikiID as string)) {
                    r.res.status(400).json({
                        error: "Wiki is not in workspace",
                    });
                    return;
                }

                // Get WikiSupport for the wiki
                const wikiSupport = WikiSupport[wikiID as string];
                if (!wikiSupport) {
                    r.res.status(400).json({
                        error: "Wiki is not supported",
                    });
                    return;
                }

                // We need an authed Axios as getting some pages may be privileged
                const authxios = APIRouter.wmfOAuth.getAuthedAxios(CRI.WMFOAuthToken);



                // Make the request to the wiki
                const result = await authxios.get(`${wikiSupport.url}/w/api.php?action=parse&page=${encodeURIComponent(pageTitle as string)}&prop=text&formatversion=2&format=json`);
                if (result.status !== 200) {
                    r.res.status(400).json({
                        error: "Error getting page",
                    });
                    return;
                }
                const { parse } = result.data;

                r.res.status(200).json({
                    ...parse,
                });
            } catch (error) {
                r.res.status(400).json({
                    error: "Workspace not found, operation not permitted or other error",
                });
            }
        },
    });
    route.run(props.req, props.res);
};