import WorkspaceHelper from "../../../helpers/WorkspaceHelper";
import AuthedRoute, { AuthedRouteExit } from "../../AuthedRoute";
import { RouteParams } from "../RouteDefs";

export default async (props: RouteParams) => {
    const route = new AuthedRoute({
        ...props,
        security: {
            SuperOverlordOnly: false,
            OverlordOnly: false,
            AdminOnly: false,
            VerifiedOnly: false,
            ApprovedUsersOnly: true, // Only approved can create a workspace
            SuspendedCanRun: false,
            LockedUsersCanRun: false,
        },
        okayRoute: async (r: AuthedRouteExit) => {
            const { res, req, db } = r;

            // Check if request is okay
            if (!req.body.title || !req.body.description) {
                res.status(400).json({
                    message: "Missing title or description"
                });
                return;
            }

            // Create workspace
            const id = await WorkspaceHelper.createWorkspace(db, req.body.title, req.body.description, r.CRI.user.userID);
            res.status(200).json({
                id
            });
        },
    });
    route.run(props.req, props.res);
};