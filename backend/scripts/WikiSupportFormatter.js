/* eslint-disable prefer-const */
// For use on metawiki in the javascript console
$.getJSON("https://meta.wikimedia.org/w/api.php?action=sitematrix&formatversion=2&format=json", cb=>{
    
    const { sitematrix } = cb;
    console.log(sitematrix);
    let finalObj = {};
    let finalLanguageArr = [];

    for (const langKey in sitematrix) {
        const lang = sitematrix[langKey];
        finalLanguageArr.push(lang.name); // Add language to array for filtering

        // If there are sites in the language
        if(lang.site) lang.site.forEach(site=>{
            if (site.closed === true) return; // don't include closed wikis
            Object.assign(finalObj, {
                [site.dbname]: {
                    langCode: lang.code,
                    langName: lang.name,
                    langNameEN: lang.localname,
                    name: site.sitename,
                    url: site.url,
                }
            });
        });
    }

    // Special wikis
    sitematrix.specials.forEach(site=>{
        console.log(site);
        if (site.nonglobal || site.closed || site.private || site.fishbowl) return; // don't include closed wikis or nonglobal wikis we can't access
        Object.assign(finalObj, {
            [site.dbname]: {
                langName: "Special",
                name: site.sitename,
                url: site.url,
            }
        });
    });

    console.log(finalObj);

    // Now language
    console.log(finalLanguageArr);
});